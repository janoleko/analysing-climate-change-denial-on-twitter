getwd()
setwd("/Users/jan-ole/Python")

tweets = read.csv("tweets_predicted.csv")

library(tidyverse)

colnames(tweets)

sum(tweets$pred_final)
# in total 72485 tweets predicted as denial 

sum(tweets$pred_final)/nrow(tweets)
# 4 percent

x = c(sum(tweets$pred_final), nrow(tweets)-c(sum(tweets$pred_final)))
pie(x, labels = c("denial", "non-denial"), col = c("red", "yellow"))

View(tweets %>% filter(nuts_3 == "DE948") %>% select(text2, pred_final))


# Get some example tweets for both categories -----------------------------

view = tweets %>% filter(pred_final == 0) %>% select(text2, pred_final) # %>% filter(stringr::str_detect(text2, "menschengemacht"))
View(view)

## group in time

tweets_time = tweets %>% 
  mutate(post_month = str_sub(post_date, 1, 7)) %>% 
  group_by(post_date) %>% 
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup() %>% 
  mutate(date = as.Date(post_date, "%Y-%m-%d")) %>% 
  arrange(date) 
  

plot(rel~date, tweets_time, xaxt = "n", type = "h", lwd = .5, main ="", ylab = "Relative frequency", xlab = "Date", bty = "n", col = "darkgrey")
axis(1, tweets_time$date, format(tweets_time$date, "%m %Y"), cex.axis = .7)
lines(ksmooth(tweets_time$date,tweets_time$rel, kernel = "normal", bandwidth = 30), col = "orange", lwd = 3)
# lines(ksmooth(tweets_time$date,tweets_time$rel, kernel = "normal", bandwidth = 90), col = "red", lwd = 3)


# Analysis ----------------------------------------------------------------

getwd()
setwd("/Users/jan-ole/R/Packages_on_Git/Tweets")

# read in INKAR data
socio = read.csv("socio.csv", sep = ";")
socio = socio[2:nrow(socio),]
nuts3 = giscoR::gisco_get_nuts(country = "Germany", nuts_level = 3) %>% select(Raumeinheit = NAME_LATN, NUTS_ID)

# some cleaning to join it to the tweet data set
nuts3$Raumeinheit = nuts3$Raumeinheit %>% str_replace_all(", Kreisfreie Stadt", "")
nuts3$Raumeinheit = nuts3$Raumeinheit %>% str_replace_all(", Stadt", "")
nuts3$Raumeinheit = nuts3$Raumeinheit %>% str_replace_all(", Landkreis", "")
nuts3$Raumeinheit = nuts3$Raumeinheit %>% str_replace_all(" \\(DE\\)", "")

socio$Raumeinheit = socio$Raumeinheit %>% str_replace_all(", Kreisfreie Stadt", "")
socio$Raumeinheit = socio$Raumeinheit %>% str_replace_all(", Stadt", "")
socio$Raumeinheit = socio$Raumeinheit %>% str_replace_all(", Landkreis", "")
socio$Raumeinheit = socio$Raumeinheit %>% str_replace_all(" \\(DE\\)", "")

d = socio %>% left_join(nuts3, by = "Raumeinheit")

d %>% filter(is.na(NUTS_ID)) %>% select(Raumeinheit)

d$Raumeinheit[which(is.na(d$NUTS_ID))]
d$NUTS_ID[which(is.na(d$NUTS_ID))] = c("DE111", "DE121", "DE125", "DE126", "DE129", "DE131", "DE144", "DE21G", "DE21J",
                                       "DE233", "DE236", "DE237", "DE24D", "DE25A", "DE277")

# nuts3$NUTS_ID[grep("Dillingen", nuts3$Raumeinheit)]
# nuts3$Raumeinheit[grep("Dillingen", nuts3$Raumeinheit)]
# 
# socio$Raumeinheit[grep("Mühldorf a.Inn", socio$Raumeinheit)]


tweets_nuts = tweets %>% 
  group_by(nuts_3) %>%
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup()


# Joining the two datasets ------------------------------------------------

colnames(tweets_nuts) = c("NUTS_ID", "count", "denial", "rel")
data = d %>% left_join(tweets_nuts, by = "NUTS_ID")

# Data cleaning -----------------------------------------------------------

for (i in 4:36){
  data[,i] = data[,i] %>% str_replace_all("\\.", "") %>% str_replace_all(",", ".") %>% as.numeric()
}

# EDA ---------------------------------------------------------------------

colnames(data)

# selecting only a subsection of predictors

data_sub = data %>% 
  mutate(sex = Bevölkerung.männlich/Bevölkerung.weiblich, 
         schulabgänger_100 = Schulabgänger.ohne.Abschluss/Bevölkerung.gesamt*100,
         arbeitslose = Arbeitslose/Bevölkerung.gesamt) %>% 
  select(Kennziffer, Raumeinheit, Aggregat, NUTS_ID,
                count, denial, rel,
                sex,
                age = Durchschnittsalter.der.Bevölkerung,
                pop_density = Einwohnerdichte,
                schulabgänger = schulabgänger_100, azubis = Auszubildende.je.100.Einwohner.15.bis.25.Jahre,
                studis = Studierende.je.100.Einwohner.18.bis.25.Jahre,
                cdu_csu = Stimmenanteile.CDU.CSU,
                spd = Stimmenanteile.SPD, 
                grüne = Stimmenanteile.Grüne,
                fdp = Stimmenanteile.FDP,
                afd = Stimmenanteile.AfD, 
                linke = Stimmenanteile.Die.Linke,
                bip = Bruttoinlandsprodukt.je.Einwohner,
                arbeitslose,
                foreigners = Ausländeranteil,
                wahlbeteiligung = Wahlbeteiligung
                ) %>% 
  mutate(pol_index = cdu_csu + 2*afd + fdp - spd - grüne - 2*linke)
data_sub$election_winner = colnames(data_sub)[apply(data_sub[,14:19], 1, which.max)+13]
data_sub$pol_index

colnames(data_sub)

# politics
plot(data_sub$cdu_csu, data_sub$rel, pch = 20)
cor(data_sub$cdu_csu, data_sub$rel)
plot(data_sub$spd, data_sub$rel, pch = 20)
cor(data_sub$spd, data_sub$rel)
plot(data_sub$fdp, data_sub$rel, pch = 20)
cor(data_sub$fdp, data_sub$rel)
plot(data_sub$grüne, data_sub$rel, pch = 20)
cor(data_sub$grüne, data_sub$rel)
plot(data_sub$afd, data_sub$rel, pch = 20)
cor(data_sub$afd, data_sub$rel)
plot(data_sub$linke, data_sub$rel, pch = 20)
cor(data_sub$linke, data_sub$rel)
# age
plot(data_sub$age, data_sub$rel, pch = 20)
cor(data_sub$age, data_sub$rel)
#sex
plot(data_sub$sex, data_sub$rel, pch = 20)
cor(data_sub$sex, data_sub$rel)
# population density
plot(data_sub$pop_density, data_sub$rel, pch = 20)
cor(data_sub$pop_density, data_sub$rel)
# bip
plot(data_sub$bip, data_sub$rel, pch = 20)
cor(data_sub$bip, data_sub$rel)
## Education
# studierende
plot(data_sub$studis, data_sub$rel, pch = 20)
cor(data_sub$studis, data_sub$rel)
# azubis
plot(data_sub$azubis, data_sub$rel, pch = 20)
cor(data_sub$azubis, data_sub$rel)

colnames(data_sub)

data_scaled = data.frame(
  data_sub[,1:7], scale(data_sub[,8:24])
)


# Model selection binomial GLM --------------------------------------------

mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + grüne + fdp + afd + linke + # politics
            age + I(age^2) + sex + pop_density + # socio
            azubis + studis + schulabgänger + # eductation
            bip + arbeitslose,
          data = data_sub, family = binomial)
summary(mod)

# exclude arbeitslose
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + grüne + fdp + afd + linke + # politics
            age + I(age^2) + sex + pop_density + # socio
            azubis + studis + schulabgänger + # eductation
            bip,
          data = data_sub, family = binomial)
summary(mod)

# exclude linke
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + grüne + fdp + afd + # politics
            age + I(age^2) + sex + pop_density + # socio
            azubis + studis + schulabgänger + # eductation
            bip,
          data = data_sub, family = binomial)
summary(mod)

# exclude sex
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + grüne + fdp + afd + # politics
            age + I(age^2) + pop_density + # socio
            azubis + studis + schulabgänger + # eductation
            bip,
          data = data_sub, family = binomial)
summary(mod)

# basically all significant, high dispersion --> quasibinomial glm!!
##############################################################################

# Histograms of pop_density and GDP skewed --> log
par(mfrow = c(1,2))
hist(data_sub$pop_density, main = "Histogram", xlab = "population density")
hist(log(data_sub$pop_density), main = "Histogram", xlab = "log population density")

hist(data_sub$bip, main = "Histogram", xlab = "GDP per cap.")
hist(log(data_sub$bip), main = "Histogram", xlab = "log GDP per cap.")

## Creating Dummy for east Germany

data_sub2 = data_sub %>% 
  mutate(nuts1 = str_sub(NUTS_ID,1,3))

data_sub$east = 0
data_sub$east[which(data_sub2$nuts1 == "DE4" | data_sub2$nuts1 == "DE8" | data_sub2$nuts1 == "DED" | data_sub2$nuts1 == "DEE" | data_sub2$nuts1 == "DEG")] = 1

## Model selection with quasibinomial GLM (backward selection)

mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + grüne + fdp + afd + linke + wahlbeteiligung +# politics
            age + sex + log(pop_density) + # socio
            azubis + studis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove grüne
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + fdp + afd + linke + wahlbeteiligung +# politics
            age + sex + log(pop_density) + # socio
            azubis + studis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove age
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + fdp + afd + linke + wahlbeteiligung +# politics
            sex + log(pop_density) + # socio
            azubis + studis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove sex
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + fdp + afd + linke + wahlbeteiligung +# politics
            log(pop_density) + # socio
            azubis + studis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove fdp
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + afd + linke + wahlbeteiligung +# politics
            log(pop_density) + # socio
            azubis + studis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove afd
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + linke + wahlbeteiligung +# politics
            log(pop_density) + # socio
            azubis + studis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove studis
mod = glm(cbind(denial, count-denial) ~ cdu_csu + spd + linke + wahlbeteiligung +# politics
            log(pop_density) + # socio
            azubis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove cdu_csu
mod = glm(cbind(denial, count-denial) ~ spd + linke + wahlbeteiligung +# politics
            log(pop_density) + # socio
            azubis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove pop_density
mod = glm(cbind(denial, count-denial) ~ spd + linke + wahlbeteiligung +# politics
            azubis + # eductation
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

# remove azubis
mod = glm(cbind(denial, count-denial) ~ spd + linke + wahlbeteiligung +# politics
            log(bip) + east,
          data = data_sub, family = quasibinomial)
summary(mod)

exp(mod$coefficients)

data_scaled = data.frame(
  data_sub[,1:7], scale(data_sub[,8:24]), east = data_sub[,26]
)

mod_scaled = glm(cbind(denial, count-denial) ~ spd + linke + wahlbeteiligung +# politics
            bip + east,
          data = data_scaled, family = quasibinomial)
summary(mod_scaled)
exp(mod_scaled$coefficients)


# Model checking ----------------------------------------------------------

## for quasibinomial model

# No likelihood but deviance

# Goodness of fit
pchisq(20651-18390, df = 438-434, lower.tail = F)
# significantly better than null model

# Badness of fit
pchisq(18390, df = 434, lower.tail = F)
# significantly worse than saturated model

## residuals
res = residuals(mod, type = "deviance")
par(mfrow = c(1,1))

par(mfrow = c(1,2))
hist(res, breaks = 40, prob = T, xlim = c(-20,20), main = "Histogram of Deviance residuals", xlab = "Deviance residuals", border = "white")
curve(dnorm(x, mean(res), sd(res)), add = T, col = "orange", lwd = 2)
qqnorm(res, main = "Normal Q-Q Plot of Deviance residuals", pch = 20, bty = "n")
qqline(res, col = "orange", lwd = 2)


# okayish
# residuals are same for both models


# Plotting ----------------------------------------------------------------

library(sf)
library(ggplot2)

de_nuts3 <- giscoR::gisco_get_nuts(country = "Germany", nuts_level = 3)

min(tweets$post_date)
max(tweets$post_date)

par(mfrow = c(1,3))

library(ggpubr)
# 2019

tweets_nuts = tweets %>% 
  mutate(post_date = as.Date(post_date)) %>% 
  filter(post_date > "2019-01-01" & post_date < "2019-12-31") %>% 
  group_by(nuts_3) %>%
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup()

colnames(tweets_nuts) = c("NUTS_ID", "count", "count_denial", "prop")

de_nuts = de_nuts3 %>%
  left_join(tweets_nuts, by = c("NUTS_ID"))

rbPal <- grDevices::colorRampPalette(c("yellow", "red"))
de_nuts$color <- rbPal(nrow(de_nuts))[as.numeric(cut(de_nuts$prop, breaks = nrow(de_nuts)))]

plot1 = ggplot(data = de_nuts) +
  geom_sf(mapping = aes(fill = color), col = "white", lwd = 1)+
  scale_fill_identity() +
  theme_void()+
  theme(plot.title = element_text(hjust = 0.5))

# 2020

tweets_nuts = tweets %>% 
  mutate(post_date = as.Date(post_date)) %>% 
  filter(post_date > "2020-01-01" & post_date < "2020-12-31") %>% 
  group_by(nuts_3) %>%
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup()

colnames(tweets_nuts) = c("NUTS_ID", "count", "count_denial", "prop")

de_nuts = de_nuts3 %>%
  left_join(tweets_nuts, by = c("NUTS_ID"))

rbPal <- grDevices::colorRampPalette(c("yellow", "red"))
de_nuts$color <- rbPal(nrow(de_nuts))[as.numeric(cut(de_nuts$prop, breaks = nrow(de_nuts)))]

plot2 = ggplot(data = de_nuts) +
  geom_sf(mapping = aes(fill = color), col = "white", lwd = 1)+
  scale_fill_identity() +
  theme_void()+
  theme(plot.title = element_text(hjust = 0.5))

# 2021

tweets_nuts = tweets %>% 
  mutate(post_date = as.Date(post_date)) %>% 
  filter(post_date > "2021-01-01" & post_date < "2021-12-31") %>% 
  group_by(nuts_3) %>%
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup()

colnames(tweets_nuts) = c("NUTS_ID", "count", "count_denial", "prop")

de_nuts = de_nuts3 %>%
  left_join(tweets_nuts, by = c("NUTS_ID"))

rbPal <- grDevices::colorRampPalette(c("yellow", "red"))
de_nuts$color <- rbPal(nrow(de_nuts))[as.numeric(cut(de_nuts$prop, breaks = nrow(de_nuts)))]

plot3 = ggplot(data = de_nuts) +
  geom_sf(mapping = aes(fill = color), colour = "white", lwd = 1)+
  scale_fill_identity() +
  theme_void()+
  theme(plot.title = element_text(hjust = 0.5))


ggarrange(plot1, plot2, plot3, 
          labels = c("2019", "2020", "2021"),
          ncol = 3, nrow = 1)



# Top regions each year ---------------------------------------------------

# overall
tweets %>% 
  group_by(nuts_3) %>%
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup() %>% 
  mutate(NUTS_ID = nuts_3) %>% 
  right_join(d, by = "NUTS_ID") %>% 
  select(nuts_3, Raumeinheit, n, count, rel) %>% 
  arrange(desc(rel))

# 2019
tweets %>% 
  mutate(post_date = as.Date(post_date)) %>% 
  filter(post_date > "2019-01-01" & post_date < "2019-12-31") %>% 
  group_by(nuts_3) %>%
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup() %>% 
  mutate(NUTS_ID = nuts_3) %>% 
  right_join(d, by = "NUTS_ID") %>% 
  select(nuts_3, Raumeinheit, n, count, rel) %>% 
  arrange(desc(rel))

# 2020
tweets %>% 
  mutate(post_date = as.Date(post_date)) %>% 
  filter(post_date > "2020-01-01" & post_date < "2020-12-31") %>% 
  group_by(nuts_3) %>%
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup() %>% 
  mutate(NUTS_ID = nuts_3) %>% 
  right_join(d, by = "NUTS_ID") %>% 
  select(nuts_3, Raumeinheit, n, count, rel) %>% 
  arrange(desc(rel))

# 2021
tweets %>% 
  mutate(post_date = as.Date(post_date)) %>% 
  filter(post_date > "2020-01-01" & post_date < "2020-12-31") %>% 
  group_by(nuts_3) %>%
  summarise(n = n(), count = sum(pred_final), rel = sum(pred_final)/n()) %>% 
  ungroup() %>% 
  mutate(NUTS_ID = nuts_3) %>% 
  right_join(d, by = "NUTS_ID") %>% 
  select(nuts_3, Raumeinheit, n, count, rel) %>% 
  arrange(desc(rel))



# Creating table with examples for correct and incorrect predictions --------

tw = tweets %>% filter(!is.na(label)) %>% select(text2, label, pred)
View(tw)

View(tw %>% filter(label == 1 & pred == 0))

View(tw[which(tw$label != tw$pred_final),])
     